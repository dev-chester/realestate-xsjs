// Names of mandatory parameters
var dbNameParam = 'dbName';
var procNameParam = 'procName';

// Prepares an array with SP parameters
// Parameters will be passed by their order. Mandatory parameters will be excluded
function getProcedureParameters()
{
	var paramList = [];
	var param; 
	var counter = 0;
	for (counter; counter < $.request.parameters.length; counter++)
	{
		param = $.request.parameters[counter];		
		if (param.name.toUpperCase() === dbNameParam.toUpperCase()
				|| param.name.toUpperCase() === procNameParam.toUpperCase())
		{
			// Exclude mandatory parameters
			continue;
		}
		
		paramList.push(param.value);
	}
	return paramList;
}

// Prepares object with the procedure text and parameters set to execute
function prepareStatement(connection, dbName, procName, params)
{
	var script = 'CALL "' + dbName + '"."' + procName + '" ({params});';
	var paramsString = '';
	// Concatenate parameters
	params.forEach(function () { paramsString = paramsString + '?, '; });
	// Remove the last comma
	paramsString = params.length > 0 ? paramsString.substring(0, paramsString.length - 2) : '';
	// Replace params string in a query template
	var call = connection.prepareStatement(script.replace('{params}', paramsString));
	// Set the params
	var counter = 1;
	params.forEach(function (el) { 
		call.setString(counter, el);
		counter = counter + 1;
	});	
	
	return call;
}

// Gets value from the record set by it's type
function getValueFromRs(recordSet, columnNum, typeCode)
{
	var result;
	switch (typeCode)
	{
		case 2:  result = recordSet.getInteger(columnNum); break; // SMALLINT
		case 3:  result = recordSet.getInteger(columnNum); break; // INTEGER
		case 5:  result = recordSet.getDecimal(columnNum); break; // DECIMAL
		case 11: result = recordSet.getString(columnNum); break; // NVARCHAR
		case 16: result = recordSet.getTimestamp(columnNum); break; // TIMESTAMP
		case 26: result = recordSet.getNClob(columnNum); break; // NCLOB
		case 27: result = recordSet.getBlob(columnNum); break; // BLOB
		case 4: result = recordSet.getString(columnNum); break; //workaround CPA
		case 14: 
			result = recordSet.getString(columnNum);
			break; // workaround CPA for date
		case 9: result = recordSet.getString(columnNum); break;
		default: throw ('Column type ' + typeCode.toString() + ' for column ' + columnNum + ' is not supported. Add type implementation to the switch statement.');
	}
	
	return result;
}

// Prepares an array with objects for each returned row
function prepareTable(recordSet)
{
	var result = [];
	var colCount = recordSet.getMetaData().getColumnCount();
	// Get columns from metadata
	var colArray = [];
	var col;
	for (col = 1; col <= colCount; col ++) 
	{
		colArray.push(
			{
				colName: recordSet.getMetaData().getColumnName(col),
				colTypeNum: recordSet.getMetaData().getColumnType(col)
			}
		);
	}
	// Fill table
	while (recordSet.next()) 
	{ 
		var newRow = {};
		var i;
		for (i = 0; i < colCount; i ++)
		{
			newRow[colArray[i].colName] = getValueFromRs(recordSet, i + 1, colArray[i].colTypeNum);
		}
		result.push(newRow); 
	} 
	return result;
}

// Connects to the provided DB and executes the procedure
function executeProcedure(dbName, procName, params)
{	
	// result
    var results = {};
    results.IsError = false;
    
    // Object to be used
    var connection;
    
    // Try to open the connection
    try
    {
        connection = $.db.getConnection();
    }
    catch(exc)
    {
        results.IsError = true;
        results.ErrorMessage = 'Cannot open connection: ' + exc;
        return results;
    }

    // Try to execute the procedure with the params provided
    try
    {
        // Prepare statement for the procedure
        var call = prepareStatement(connection, dbName, procName, params);
        // Execute
        call.execute();
        var resultSet = call.getResultSet();
        // Convert result set to an array with objects
        try
        {
            results.Data = prepareTable(resultSet);
            }
        finally
        {
            // Close
            resultSet.close(); 
            call.close(); 
            }
        }
    catch(exc2)
    {
        results.IsError = true;
        results.ErrorMessage = 'Cannot execute procedure ' + procName + ' in schema ' + dbName + ': ' + exc2;
        return results;
    }
    finally
    {
        connection.close(); 
    }

    return results;
}

// Request process 
function processRequest()
{
	// Allways JSON
	$.response.contentType = "application/json";
	    
	var result; 
	
	//Build the response
    $.response.status = $.net.http.OK;
	
	try
	{
		var dbName = $.request.parameters.get(dbNameParam);
		var procName = $.request.parameters.get(procNameParam);
		if (!(dbName && procName))
		{
			// Mandatory parameters have not been passed - bad request
			throw ('Parameters "' + dbNameParam + '" and "' + procNameParam + '" must be presented!');
		}
		
		var procParams = getProcedureParameters();		
		
		// Try to execute the procedure
		var procResult = executeProcedure(dbName, procName, procParams);
		if (procResult.IsError === false)
		{
			result = JSON.stringify(procResult.Data); // Always get the first property
		}
		else
		{
			$.response.status = $.net.http.BAD_REQUEST;
			result = JSON.stringify({ ErrorMessage: procResult.ErrorMessage });
		}
	}
	catch(exc)
	{
		result = JSON.stringify({ ErrorMessage: exc });
		$.response.status = $.net.http.BAD_REQUEST;
	}
    
    $.response.setBody(result);
   
}

// Call request processing  
processRequest();
