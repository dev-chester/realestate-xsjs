/*
 * Filename		:	PostData.xsjs
 * 
 * Description	:	Receive POST method request and simply insert data into respective tables.
 * 
 * Requirements	:	For JSON object; level 1 properties/keys are always the TABLENAME;
 *					values should be in array inside array is objects and will treat objects as ROWS;
 *					object properties in inside array should have properties mapped to COLUMNS.
 * 
 * Created By	: Chester Arellano
 * 
 * -------------Update the changelog!-------------
 * Version Date	: 05/09/2019 v1 
 * 
 * Version Date : 05/23/2019 v2
 *     Can accomodate update / insert. fix update / insert for Date fields like (BirthDate, DueDate)
 *     Automatic update / insert in CreatedDate / UpdatedDate and CreatedBy / UpdatedBy
 * 
 * */



var sTables = [];
var sInsertStatements = [];
var oRow = {};
var result = "";
var conn = $.hdb.getConnection();

//get first level object property count for table operations, done
function getTableCount(){ 
	return  Object.keys(JSON.parse($.request.body.asString())).length;  
}

//get all object property keys for table operations, done
function getAllTableInserts(){ 
	 var keys = Object.keys(JSON.parse($.request.body.asString()));
	 return keys;
}

//prepare insert statements, done
function composeInsertStatement(){
	var iDebug = 0;
	try{
		var aRequestBody = JSON.parse($.request.body.asString()); 
		
		var aTables = getAllTableInserts(); //Array of tables
		var i = 0;
		var sPreInsert = "";
		for (i=0; i<getTableCount(); i++) { //all tables to be inserted/updated
			
			
			//Number of Operations
			var iTableRows = aRequestBody[Object.keys(aRequestBody)[i]].length;

			var sInsertTable = aTables[i];
			var j = 0; 
			for (j=0; j< iTableRows; j++){
				
				// here retrieve the object / row
				var oRow = aRequestBody[Object.keys(aRequestBody)[i]][j];
				//Determine what operation to be use, Update / Insert
				var sOperation = oRow.O;
				sPreInsert = "";
				var oColumns = [];
				var index = 0;
				if (sOperation === 'I'){
					try{
						//Insert operation
						sPreInsert = "INSERT INTO " + sInsertTable + "(";
						
						// Specify columns in Insert
						oColumns = Object.keys(oRow);
					
						for (index = 0; index < oColumns.length; ++index) {
							if (oColumns[index] === 'O'){
								continue;
							}
							sPreInsert = sPreInsert + '"' + oColumns[index] + '",';
						}
						sPreInsert = sPreInsert + ' "CreatedBy", "CreatedDate" ) VALUES (';
						
						
						// Specify values in Insert
						var oValues =[];
						for (index = 0; index < oColumns.length; ++index) {
							try{
								if (oColumns[index].match(/Date/gi) !== null){
									oValues.push("TO_DATE('"+oRow[Object.keys(oRow)[index]]+"', 'MM/dd/YYYY')");	
								}else{
									oValues.push("'"+oRow[Object.keys(oRow)[index]]+"'");	
								}
							}catch(gfg){
								oValues.push(gfg.toString());
							}											
						} 
						
						oValues.push("'manager'");
						oValues.push("(SELECT CURRENT_TIMESTAMP FROM DUMMY)");
						
						//$.response.setBody(oValues.toString());
						
						for (index = 0; index < oValues.length; ++index) {
							if (oColumns[index] === 'O'){
								continue;
							}
							sPreInsert = sPreInsert + ' ' + oValues[index] + ',';
						}
						sPreInsert = (sPreInsert.substr(0, sPreInsert.length-1)) + ");";
					}catch(err){
						sInsertStatements.push("error on Insert");
					}
					
				}else{
					//Update operation
					var preUpdate = "UPDATE " + sInsertTable + " ";
					preUpdate = preUpdate + " SET ";
					oColumns = Object.keys(oRow);
					index = 0;
					for (index = 0; index < oColumns.length; ++index) {
						if (oColumns[index] === 'O' || oColumns[index] === 'Code'){
							continue;
						}
						try{
							if (oColumns[index].match(/Date/gi) !== null){
								preUpdate = preUpdate + ' "' + oColumns[index] + '" = TO_DATE(\'' + oRow[oColumns[index]] + '\',\'MM/dd/YYYY\'), ';
							}else{
								preUpdate = preUpdate + ' "'+ oColumns[index] + '" = \'' + oRow[oColumns[index]] + "', " ;
							}
						}catch(ee){
							sInsertStatements.push(ee.toString());
						}
						
							
					}
					
					//updatedBy and UpdatedDate
					preUpdate = preUpdate + ' "UpdatedBy" = \'manager\' , "UpdatedDate" = (SELECT CURRENT_TIMESTAMP FROM DUMMY) ';
					preUpdate = preUpdate + " WHERE \"Code\" = '" + oRow.Code+ "';";
					sPreInsert = preUpdate;
				}
				
				//add insertStatement to array
				sInsertStatements.push(sPreInsert);
			}//END OF TableRows
		
		}
		
	}catch(err2){
		iDebug = err2.toString();
		$.response.setBody(iDebug);
	}	
}

//actual posting or execution of insert statements generated, done
function executePostings(){
	try{ 
		
		conn.executeUpdate("SET SCHEMA \"" + $.request.parameters.get("dbName") + "\"");
		var i = 0;
		for(i = 0; i < sInsertStatements.length; i++){
			conn.executeUpdate(sInsertStatements[i].toString());
		}
		return 0;
	}catch(err){
		$.response.setBody(err.toString());
		return -1;
		
	}
}
 
function entryPoint(){

	$.response.contentType = "application/json";
 
	$.response.status = $.net.http.OK;
	try{
		var dbName = $.request.parameters.get("dbName");
		if (!(dbName)){
			//Make parameter schema mandatory
			throw ("Parameter dbName is Required!");
		}else{
			composeInsertStatement();
			
			//$.response.setBody(dateTime.toLocaleDateString() + ' ' + dateTime.toLocaleTimeString());
			
			//$.response.setBody(sInsertStatements.toString());
			
			if (executePostings() === 0){
				conn.commit();
				conn.close();
				$.response.status = $.net.http.OK;				
				$.response.setBody(JSON.stringify({Result: "Success"}));
			}else{
				conn.rollback();
				conn.close();
				$.response.status = $.net.http.BAD_REQUEST;			
			}
		}
		
	}catch(err){
		$.response.setBody(JSON.stringify({ ErrorMessage: err }));
		$.response.status = $.net.http.BAD_REQUEST;
	}
	
}
entryPoint();


